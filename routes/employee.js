const router = express.Router()

const EmplyeeController = require('../controllers/EmployeeControllers')

router.get('/', EmplyeeController.index)
router.post('/show', EmplyeeController.show)
router.post('/store', EmplyeeController.store)
router.post('/update', EmplyeeController.update)
router.post('/destroy', EmplyeeController.destroy)

module.exports = router
