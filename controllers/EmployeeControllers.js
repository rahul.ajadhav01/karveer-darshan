const { response } = require('express')
const Employee = require('../modules/Employee')

const index = (req, res, next) => {
    Employee.find()
    .then(response => {
        res.json({
            response
        })
    })
    .catch(err => {
        res.json({
            message: 'An Error Ocured!'
        })
    })
}

const show = (req, res, next) => {
    let employeeID = req.body.employeeID
    Employee.findById(employeeID)
    .then(response => {
        res.json({
            response
        })
    })
    .catch(error => {
        res.json({
            message: 'An Error Ocured!'
        })
    })
}

const store = (req, res, next) => {
    let employee = new Employee({
        name: req.body.name,
        designation: req.body.designation,
        email: req.body.email,
        phone: req.body.phone,
        age: req.body.age,
    })
    employee.save()
    .then(response => {
        res.json({
            message: "Employee Added Succesfully!"
        })
    })
    .catch(error => {
        res.json({
            message: 'An error Occured!'
        })
    })
}

const update = (req, res, next) => {
    let employeeID = req.body.employeeID

    let updateData = {
        name: req.body.name,
        designation: req.body.designation,
        email: req.body.email,
        phone: req.body.phone,
        age: req.body.age,
    }
    Employee.findByIdAndUpdate(employeeID, {$set:updateData})
    .then(() => {
        res.json({
            message: "Employee Update Successfully!"
        })
    })
    .catch(error => {
        res.json({
            message: 'An error Occured!'
        })
    })
}

const destroy = (req, res, next) => {
    let employeeID = req.body.employeeID
    Employee.findByIdAndRemove(employeeID)
    .then(() => {
        res.json({
            message: "Delete Employee Successfully!"
        })
    })
    .catch(error => {
        res.json({
            message: "An Error Occured"
        })
    })
}

module.exports = {
    index, show, store, update, destroy
}