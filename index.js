const express      = require('express')
const mongoose     = require('mongoose')
const morgan       = require('morgan')
const bodyParser   = require('body-parser')

mongoose.connect('mongodb://localhost:27017/testdb', { useNewUrlParser: true, useUnifiedTopology: true })
const db = mongoose.connection

const EmployeeRoute = require('./routes/employee')
const AuthRoute = require('./routes/auth')

db.on('Error', (err) => {
    console.log(err)
}) 

db.once('Open', () => {
    consol.log('Database Connection Established!')
})

const app = express()

app.use(morgan('dev'))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const PORT = process.env.PORT || 100

app.listen(PORT, () => {
    console.log(`The Server is Run On PORT ${PORT}`)
})

app.use('/api/employee', EmployeeRoute)
app.use('/api', AuthRoute)